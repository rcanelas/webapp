exports.relativeTime = function (dateTime) {
  const d = typeof dateTime === "string" ? new Date(dateTime) : dateTime;
  const secondsSince = Math.floor((new Date() - d) / 1000);
  let humanText = "";
  if (secondsSince < 60) {
    humanText = "just now";
  } else if (secondsSince < 3600) {
    humanText = Math.floor(secondsSince / 60) + "m ago";
  } else if (secondsSince < 86400) {
    humanText = Math.floor(secondsSince / 3600) + "h ago";
  } else if (secondsSince < 604800) {
    humanText = Math.floor(secondsSince / 86400) + "d ago";
  } else if (secondsSince < 2419200 * 2 + 604800) {
    // max 9 weeks
    humanText = Math.floor(secondsSince / 604800) + "w ago";
  } else if (secondsSince < 31449600) {
    humanText = Math.floor(secondsSince / 2419200) + " months ago";
  } else if (secondsSince < 31449600 * 10) {
    humanText = Math.floor(secondsSince / 31449600) + "y ago";
  } else {
    // after ~ 10 years
    humanText = d.toLocaleDateString("en-GB");
  }
  return humanText;
};
